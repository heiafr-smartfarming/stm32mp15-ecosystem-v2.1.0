# Smartfarming

@Author : Amanda Hayoz

@School : HES-SO, Switzerland

@Institute : iSIS, HEIA-FR, Fribourg, Switzerland

@Description : Semester project done during second semester of the master MES in computer sciences (orientation : embedded and mobile systems).

---------------------------------------------------------------------------------------------------
This repository contains the source code created during the project. For documentation such as the final report, refer to the following repository : https://gitlab.forge.hefr.ch/heiafr-smartfarming/documentation .

---------------------------------------------------------------------------------------------------

## Context
The aim of the project was to study the possibilities offered by devices possessing two processors : a cortex-A7 where Linux can run and a cortex-M4 where bare-metal code is run.

The implemented use case is as follow :
1. Linux boots (blue LED doing heartbeat).
2. After Linux finished to boot, it initializes the system : launching program on the cortex-M (red LED turns on) and setting the wake up source.
3. Linux takes a picture, analyze it and put the cortex-M to sleep (red LED turns off) before sleeping itself as well (blue LED off).
4. When the user presses the PA14 button, it wakes the cortex-M up (red LED turns on) and then Linux wakes up as well (blue LED on).
5. Go to point 3. And so on.

This use case can be launched with the files stored inside `system` directory.

During this project, the low power modes were studied as well and the power consumption was measured with the help of the [Joulescope](https://www.joulescope.com/). For this, a shell script and a service were written to make the system go from one power mode to another after each reset, until all power modes were done. The order is as follow :
- Idle
- CRun
- LP-Stop
- Standby DDR self-refresh
- Standby DDR off

This use case can be launched with the files stored inside `power` directory.

---------------------------------------------------------------------------------------------------
## Prerequisites
The best is to have the full environment system installed.
- The starter package is installed on host machine.
- The development package is installed on host machine.
- STM32CubeIDE is installed on host machine.
- Visual Studio Code is installed on host machine.
- The SD card of the target is flashed with the starter package.
- The X-LINUX-AI expansion package is installed on the target.

The steps of these installations are fully explained in the final report. This repository already contains the necessary zip files used to setup the developement environment.

Material :
- STM32MP157A-EV1 (it should be able to run on similar cards but it wasn't tested).
- Ethernet cable (+ eventually Ethernet to USB adapter).
- Micro USB to USB (for ST-Link)
- 5V AC adapter
---------------------------------------------------------------------------------------------------

## Launch applications
Before launching the `system` application, we need to build the cortex-M application. 
1. Go in `Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Applications/OpenAMP/OpenAMP_wakeup/STM32CubeIDE` 
2. Double-click the `.project` file. It will automatically open the project inside STM32CubeIDE. 
3. Build the project.

The easiest way to launch either the `system` or `power` applications is to :
4. Double-click the `Developer-Package/workspace/environment_launch` file. It will automatically open Visual Studio Code and setup some necessary environment variables indicating the applications paths and target IP address. You might take a look inside in case paths are different.

Then, with Visual Studio Code, you can use the following tasks :
- `system-TARGET` : deletes system files on target, deploy them again and launches the services.
- `power-TARGET` : deletes power files on target, deploy them again and launches the services.

To see information and other possible actions, run :
```
Developer-Package/workspace/.vscode/system-TARGET.sh -h
Developer-Package/workspace/.vscode/power-TARGET.sh -h
```

Other Visual Studio Code tasks are already preconfigured :
- `system-TARGET-clear` : deletes all related system application files on target.
- `power-TARGET` : deletes all related power application files on target.
- `hello_world-TARGET` : compile and copy the hello world application on target.
---------------------------------------------------------------------------------------------------

## Repository files hierarchy
The following briefly explain the general structure of the repository (not all files are explained but the most important one), which is the structure obtained after all preconised installation from STMicroelectronics.

```
📦STM32MP15-Ecosystem-v2.1.0
 ┣ 📂Developer-Package [#Package used to develope applications #]
 ┃ ┣ 📂SDK [# Contains the necessary files for the cross-compilation #]
 ┃ ┣ 📂STM32Cube_FW_MP1_V1.4.0 [# Directory containing the drivers for the cortex-M as well as examples #]
 ┃ ┃ ┣ 📜Projects/STM32MP157C-EV1/Applications/OpenAMP/OpenAMP_TTY_echo_wakeup [# Example given by STMicroelectronics to understand interprocessus communication mechanism and sleep mode change #]
 ┃ ┃ ┗ 📜Projects/STM32MP157C-EV1/Applications/OpenAMP/OpenAMP_wakeup [# Cortex-M application developed during this project, based on the previous one #]
 ┃ ┣ 📂workspace [# Source code developed during this project, see next section #]
 ┃ ┗ 📜en.SOURCES-kernel-stm32mp1-openstlinux-5-4-dunfell-mp1-20-11-12.tar.xz [# Tar containing the 
 OpenSTLinux kernel #]
 ┣ 📂Starter-Package [# Package used to flash the SD card #]
 ┃ ┗ 📜en.FLASH-stm32mp1-openstlinux-5-4-dunfell-mp1-20-11-12.tar.xz [# Tar containing the Starter-Package #]
```

The compressed files are placed at right place. Normally you can extract them right where they are to get the right final files hierarchy as preconised by STMicroelectronics.

## Workspace files hierarchy
The following briefly explain the role of each file from `workspace` directory.

```
📦workspace
 ┣ 📂.vscode
 ┃ ┣ 📜launch.json [# To debug with visual studio code #]
 ┃ ┣ 📜power-TARGET .sh [# Permits to deploy and run power files on target #]
 ┃ ┣ 📜system-TARGET .sh [#  Permits to deploy and run system files on target #]
 ┃ ┗ 📜tasks.json [# Automatisation of tasks in visual studio code #]
 ┣ 📂hello_world [# Hello world sample to test installation #]
 ┃ ┣ 📜Makefile
 ┃ ┗ 📜hello_world.c
 ┣ 📂power [# Files to measure power consumption in different states #]
 ┃ ┣ 📜smartfarming_power.service [# Service launched after boot #]
 ┃ ┗ 📜smartfarming_power.sh [# Shell script changing power target mode #]
 ┣ 📂system [# Files used for the setup of the whole system : power management, images capture, images analyze, etc. #]
 ┃ ┣ 📂OpenAMP_wakeup_CM4 [# Scripts to launch cortex-M program and changing processors power mode #]
 ┃ ┃ ┣ 📜README
 ┃ ┃ ┣ 📜fw_cortex_m4.sh [# Shell script to load program on cortex-M #]
 ┃ ┃ ┣ 📜smartfarming_system_prelaunch.sh [# Launching fw_cortex_m4.sh and setting wake up source #]
 ┃ ┃ ┗ 📜smartfarming_system_wakeup.sh [#  Launching launch_python_label_rfl_mobilenet_testdata.sh and making processors sleep #]
 ┃ ┣ 📂models
 ┃ ┃ ┗ 📂mobilenet
 ┃ ┃ ┃ ┣ 📂testdata [# Directory where the captured photo are stored #]
 ┃ ┃ ┃ ┣ 📜labels.txt [# Labels of the neural network #]
 ┃ ┃ ┃ ┗ 📜mobilenet_v1_0.5_128_quant.tflite [# Neural network file  #]
 ┃ ┣ 📂python : [# Program to capture a photo and analyze it #]
 ┃ ┃ ┣ 📜README
 ┃ ┃ ┣ 📜label_tfl_multiprocessing.py [# Python program #]
 ┃ ┃ ┗ 📜launch_python_label_tfl_mobilenet_testdata.sh [# Entry point to launch Python program with necessary parameters #]
 ┃ ┗ 📂scripts [# Services to launch scripts after boot or wakup #]
 ┃ ┃ ┣ 📜smartfarming_system_prelaunch.service [# Service launched after boot #]
 ┃ ┃ ┗ 📜smartfarming_system_wakeup.service [# Service launched ater smartfarming_system_prelaunch and/or wake up #]
 ┗ 📜environment_launch [# Script you can double click to launch visual studio code with right env variables #]
 ```
