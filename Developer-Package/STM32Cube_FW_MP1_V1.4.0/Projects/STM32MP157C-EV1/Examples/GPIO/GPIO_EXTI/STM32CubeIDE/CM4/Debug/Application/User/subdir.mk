################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Examples/GPIO/GPIO_EXTI/Src/lock_resource.c \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Examples/GPIO/GPIO_EXTI/Src/main.c \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Examples/GPIO/GPIO_EXTI/Src/stm32mp1xx_hal_msp.c \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Examples/GPIO/GPIO_EXTI/Src/stm32mp1xx_it.c 

OBJS += \
./Application/User/lock_resource.o \
./Application/User/main.o \
./Application/User/stm32mp1xx_hal_msp.o \
./Application/User/stm32mp1xx_it.o 

C_DEPS += \
./Application/User/lock_resource.d \
./Application/User/main.d \
./Application/User/stm32mp1xx_hal_msp.d \
./Application/User/stm32mp1xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
Application/User/lock_resource.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Examples/GPIO/GPIO_EXTI/Src/lock_resource.c Application/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -O0 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Application/User/lock_resource.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Application/User/main.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Examples/GPIO/GPIO_EXTI/Src/main.c Application/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -O0 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Application/User/main.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Application/User/stm32mp1xx_hal_msp.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Examples/GPIO/GPIO_EXTI/Src/stm32mp1xx_hal_msp.c Application/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -O0 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Application/User/stm32mp1xx_hal_msp.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Application/User/stm32mp1xx_it.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Examples/GPIO/GPIO_EXTI/Src/stm32mp1xx_it.c Application/User/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -O0 -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Application/User/stm32mp1xx_it.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

