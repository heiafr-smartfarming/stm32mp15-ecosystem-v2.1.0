################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg/rpmsg.c \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg/rpmsg_virtio.c 

OBJS += \
./Middlewares/OpenAMP/open-amp/rpmsg/rpmsg.o \
./Middlewares/OpenAMP/open-amp/rpmsg/rpmsg_virtio.o 

C_DEPS += \
./Middlewares/OpenAMP/open-amp/rpmsg/rpmsg.d \
./Middlewares/OpenAMP/open-amp/rpmsg/rpmsg_virtio.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/OpenAMP/open-amp/rpmsg/rpmsg.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg/rpmsg.c Middlewares/OpenAMP/open-amp/rpmsg/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_FULL_LL_DRIVER -DNO_ATOMIC_64_SUPPORT -DUSE_HAL_DRIVER -DSTM32MP157Cxx -DMETAL_INTERNAL '-DMETAL_MAX_DEVICE_REGIONS=2' -DVIRTIO_SLAVE_ONLY -D__LOG_TRACE_IO_ -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/open-amp/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/virtual_driver -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Middlewares/OpenAMP/open-amp/rpmsg/rpmsg.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Middlewares/OpenAMP/open-amp/rpmsg/rpmsg_virtio.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Middlewares/Third_Party/OpenAMP/open-amp/lib/rpmsg/rpmsg_virtio.c Middlewares/OpenAMP/open-amp/rpmsg/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_FULL_LL_DRIVER -DNO_ATOMIC_64_SUPPORT -DUSE_HAL_DRIVER -DSTM32MP157Cxx -DMETAL_INTERNAL '-DMETAL_MAX_DEVICE_REGIONS=2' -DVIRTIO_SLAVE_ONLY -D__LOG_TRACE_IO_ -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/open-amp/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/virtual_driver -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Middlewares/OpenAMP/open-amp/rpmsg/rpmsg_virtio.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

