################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Drivers/BSP/STM32MP15xx_EVAL/stm32mp15xx_eval.c \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Drivers/BSP/STM32MP15xx_EVAL/stm32mp15xx_eval_bus.c \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Drivers/BSP/STM32MP15xx_EVAL/stm32mp15xx_eval_stpmic1.c 

OBJS += \
./Drivers/BSP/stm32mp15xx_eval.o \
./Drivers/BSP/stm32mp15xx_eval_bus.o \
./Drivers/BSP/stm32mp15xx_eval_stpmic1.o 

C_DEPS += \
./Drivers/BSP/stm32mp15xx_eval.d \
./Drivers/BSP/stm32mp15xx_eval_bus.d \
./Drivers/BSP/stm32mp15xx_eval_stpmic1.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BSP/stm32mp15xx_eval.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Drivers/BSP/STM32MP15xx_EVAL/stm32mp15xx_eval.c Drivers/BSP/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_FULL_LL_DRIVER -DNO_ATOMIC_64_SUPPORT -DUSE_HAL_DRIVER -DSTM32MP157Cxx -DMETAL_INTERNAL '-DMETAL_MAX_DEVICE_REGIONS=2' -DVIRTIO_SLAVE_ONLY -D__LOG_TRACE_IO_ -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/open-amp/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/virtual_driver -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/BSP/stm32mp15xx_eval.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/BSP/stm32mp15xx_eval_bus.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Drivers/BSP/STM32MP15xx_EVAL/stm32mp15xx_eval_bus.c Drivers/BSP/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_FULL_LL_DRIVER -DNO_ATOMIC_64_SUPPORT -DUSE_HAL_DRIVER -DSTM32MP157Cxx -DMETAL_INTERNAL '-DMETAL_MAX_DEVICE_REGIONS=2' -DVIRTIO_SLAVE_ONLY -D__LOG_TRACE_IO_ -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/open-amp/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/virtual_driver -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/BSP/stm32mp15xx_eval_bus.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"
Drivers/BSP/stm32mp15xx_eval_stpmic1.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Drivers/BSP/STM32MP15xx_EVAL/stm32mp15xx_eval_stpmic1.c Drivers/BSP/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_FULL_LL_DRIVER -DNO_ATOMIC_64_SUPPORT -DUSE_HAL_DRIVER -DSTM32MP157Cxx -DMETAL_INTERNAL '-DMETAL_MAX_DEVICE_REGIONS=2' -DVIRTIO_SLAVE_ONLY -D__LOG_TRACE_IO_ -c -I../../../Inc -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/open-amp/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/virtual_driver -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Drivers/BSP/stm32mp15xx_eval_stpmic1.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

