################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

ELF_SRCS := 
OBJ_SRCS := 
S_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
OBJCOPY_HEX := 
SIZE_OUTPUT := 
OBJDUMP_LIST := 
EXECUTABLES := 
OBJS := 
S_DEPS := 
S_UPPER_DEPS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Application/Startup \
Application/User \
Drivers/BSP \
Drivers/CMSIS \
Drivers/STM32MP1xx_HAL_Driver \
Middlewares/OpenAMP/libmetal \
Middlewares/OpenAMP/libmetal/generic \
Middlewares/OpenAMP/libmetal/generic/cortexm \
Middlewares/OpenAMP/open-amp/remoteproc \
Middlewares/OpenAMP/open-amp/rpmsg \
Middlewares/OpenAMP/open-amp/virtio \
Middlewares/OpenAMP/virtual_driver \

