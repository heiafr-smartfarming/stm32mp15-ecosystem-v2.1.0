/**
  @page OpenAMP_wakeup OpenAMP wake up example

  @verbatim
  ******************************************************************************
  * @file    OpenAMP/OpenAMP_wakeup/readme.txt
  * @author  Amanda Hayoz
  * @brief   Description of the OpenAMP wake up application.
  ******************************************************************************
  @endverbatim

@par Application Description

How to use OpenAMP MW to enter in different power system operating mode (Run, Stop and Standby).
 
This project deals with CPU2 (Cortex-M4) firmware and requires Linux OS running on CPU1 (Cortex-A7)
OpenAMP MW uses the following HW resources
    * IPCC peripheral for event signal (mailbox) between CPU1(CA7) and CPU2(CM4)
    * MCUSRAM peripheral for buffer communications (virtio buffers) between CPU1(CA7) and CPU2(CM4)
            Reserved shared memeory region for this example: SHM_ADDR=0x10040000 and SHM_SIZE=128k.
            It is defined in platform_info.c file

In this example:
    - CPU1(CA7 aka Master processor) loads and powers on CPU2(CM4 aka Remote processor)
                through remoteproc framework in Linux OS. System clock
                configuration is done only once by CPU1(CA7)
    - CPU2(CM4) initializes OPenAMP MW which initializes/configures IPCC peripheral
                through HAL and setup openamp-rpmsg framwork infrastructure
                (1st level of communication btween CPU1(CA7) and CPU2(CM4)
    - CPU2(CM4) creates an rpmsg channel for the virtual UART instance UART0
    - CPU2(CM4) is waiting for messages from CPU1(CA7) on this channel
        *  When CPU2(CM4) receives a message on the Virtual UART instance/rpmsg channel,
           it sends the message back to CPU1(CA7) on that Virtual UART instance
    - Some messages are handled specifically by CM4:
        - "*stop"    : upon reception of this message, CM4 goes to CStop mode and only allow entering System Stop mode
        - "*standby" : upon reception of this message, CM4 goes to CStop mode and allow entering System Stop or Standby mode.
        - "*delay"   : CM4 sends a RPMsg message to CA7 after a 20 second delay. It is a Wakeup source for CA7


    Notes:
    - It requires Linux console to start & run example.
    - CPU2(CM4) logging is redirected in Shared memory in MCUSRAM and can be displayed in Linux console for verdict
      using following command:
          cat /sys/kernel/debug/remoteproc/remoteproc0/trace0

    Following command should be done in Linux console on CA7 to run the example :

    > ./fw_cortex_m4.sh start
    > stty -onlcr -echo -F /dev/ttyRPMSG0
    > cat /dev/ttyRPMSG0 &
    > echo "Hello Virtual UART0" >/dev/ttyRPMSG0

    You should get "Hello Virtual UART0" in Linux console


----------------------------------------------
- The following operating mode can be tested -
----------------------------------------------

For all scenarios, read the readme.txt of th eOpenA;P_TTY_echo_wakeup example.

The following scenario is the one specially setup for our use case.

1) System Stop Mode with CA7 in CStop and CM4 in CStop - CM4 wakes up first
   -----------------------------------------------------------------------
    Objective: Testing System Stop Mode with CM4 waking up first.
               CM4 wakes up with PA14 button which raises Exti line 14.
               CA7 wakes up with CM4 seding a RPmsg which raises an IPCC interruption.

    1.0 - CA7 firmware has configured Exti line 14 as wake up source
          CM4 firmware has configured Exti line 62 (IPCC interrupt CPU2) as a wake up source

    1.1 - Move CM4 in CStop mode
          > echo "*stop" >/dev/ttyRPMSG0

          On reception of this message, CM4 turns LED4 on and calls 
          HAL_PWR_EnterSTOPMode(PWR_MAINREGULATOR_ON, PWR_STOPENTRY_WFI) and goes into CStop 
          mode.
          
    1.2 - Move CA7 in CStop mode
          > systemctl suspend

          In that case, this command moves CA7 in CStop mode and we enter in System Stop Mode
          as CM4 is in CStop.
          LED5 stops toggling (Linux heartbeat).

    1.3 - Move CM4 in CRun mode using User push button (PA14)

		  LED4 turns on. A RPMsg is sent to CA7 waking him up.
          LED5 toggles (Linux heartbeat)

    Note: you can check CM4 logs :
          > cat /sys/kernel/debug/remoteproc/remoteproc0/trace0

Other peripherals used:
  1 GPIO for LED
  1 GPIO for push button

STM32MP157C-EV1 board LED4 is be used to monitor the program execution status:
 - Normal operation: LED4 is turned-on when CM4 is running
 - Sleep operation: LED4 is turned-off when CM4 is sleeping

@note Care must be taken when using HAL_Delay(), this function provides accurate
      delay (in milliseconds) based on a variable incremented in SysTick ISR.
      This implies that if HAL_Delay() is called from a peripheral ISR process, then
      the HAL time base interrupt must have higher priority (numerically lower) than
      the peripheral interrupt. Otherwise the caller ISR process will be blocked.
      To change the HAL time base interrupt priority you have to use HAL_NVIC_SetPriority()
      function.
      In STM32Cube firmware packages, the SysTick timer is used as default time base,
      but it can be changed by user by utilizing other time base IPs such as a
      general-purpose timer, keeping in mind that the time base duration must be
      kept at 1/10/100 ms since all PPP_TIMEOUT_VALUEs are defined and handled
      in milliseconds. Functions affecting time base configurations are declared
      as __Weak to allow different implementations in the user file.

@note The application needs to ensure that the HAL time base is always set to 1 millisecond
      to have correct HAL operation.


@par Directory contents
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Inc/main.h                 Main program header file
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Inc/mbox_ipcc.h            mailbox_ipcc_if.c MiddleWare configuration header file
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Inc/openamp.h              User OpenAMP init header file
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Inc/openamp_conf.h         Configuration file for OpenAMP MW
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Inc/rsc_table.h            Resource_table for OpenAMP header file
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Inc/stm32mp1xx_hal_conf.h  HAL Library Configuration file
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Inc/stm32mp1xx_it.h        Interrupt handlers header file
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Src/main.c                 Main program
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Src/mbox_ipcc.c            mailbox_ipcc_if.c MiddleWare configuration
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Src/openamp.c              User OpenAMP init
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Src/rsc_table.c            Resource_table for OpenAMP
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Src/stm32mp1xx_it.c        Interrupt handlers
    - OpenAMP/OpenAMP_TTY_echo_wakeup/Src/system_stm32mp1xx.c    STM32MP1xx system clock configuration file


@par Hardware and Software environment

  - This example runs on STM32MP157CAAx devices.

  - This example has been tested with STM32MP157A-EV1 board and can be
    easily tailored to any other supported device and development board.

@par How to use it ?

In order to make the program work, you must do the following:
 - Open your preferred toolchain
 - Rebuild all files and load your image into target memory
 - Run the example


 * <h3><center>&copy; COPYRIGHT STMicroelectronics</center></h3>
 */
