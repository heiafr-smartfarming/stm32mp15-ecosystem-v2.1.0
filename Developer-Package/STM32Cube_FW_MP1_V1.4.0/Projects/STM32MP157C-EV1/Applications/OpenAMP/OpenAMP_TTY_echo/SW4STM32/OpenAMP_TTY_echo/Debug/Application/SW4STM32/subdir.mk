################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (9-2020-q2-update)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Applications/OpenAMP/OpenAMP_TTY_echo/SW4STM32/startup_stm32mp15xx.s 

C_SRCS += \
/home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Applications/OpenAMP/OpenAMP_TTY_echo/SW4STM32/syscalls.c 

OBJS += \
./Application/SW4STM32/startup_stm32mp15xx.o \
./Application/SW4STM32/syscalls.o 

S_DEPS += \
./Application/SW4STM32/startup_stm32mp15xx.d 

C_DEPS += \
./Application/SW4STM32/syscalls.d 


# Each subdirectory must supply rules for building sources it contributes
Application/SW4STM32/startup_stm32mp15xx.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Applications/OpenAMP/OpenAMP_TTY_echo/SW4STM32/startup_stm32mp15xx.s Application/SW4STM32/subdir.mk
	arm-none-eabi-gcc -mcpu=cortex-m4 -g3 -c -x assembler-with-cpp -MMD -MP -MF"Application/SW4STM32/startup_stm32mp15xx.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@" "$<"
Application/SW4STM32/syscalls.o: /home/pa-smartfarming/STM32MP15-Ecosystem-v2.1.0/Developer-Package/STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Applications/OpenAMP/OpenAMP_TTY_echo/SW4STM32/syscalls.c Application/SW4STM32/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DCORE_CM4 -DUSE_HAL_DRIVER -DSTM32MP157Cxx -DMETAL_INTERNAL '-DMETAL_MAX_DEVICE_REGIONS=2' -DMETAL_INTERNAL -DVIRTIO_SLAVE_ONLY -D__LOG_TRACE_IO_ -DNO_ATOMIC_64_SUPPORT -c -I../../../Inc -I../../../../../../../../Middlewares/Third_Party/OpenAMP/open-amp/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/virtual_driver -I../../../../../../../../Drivers/BSP/STM32MP15xx_EVAL -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc -I../../../../../../../../Drivers/STM32MP1xx_HAL_Driver/Inc/Legacy -I../../../../../../../../Drivers/CMSIS/Device/ST/STM32MP1xx/Include -I../../../../../../../../Drivers/CMSIS/Include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/open-amp/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/libmetal/lib/include -I../../../../../../../../Middlewares/Third_Party/OpenAMP/virtual_driver -Og -ffunction-sections -Wall -fstack-usage -MMD -MP -MF"Application/SW4STM32/syscalls.d" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

