#!/bin/sh

# Application to mesure consumption for different power mode.
# On the first run, the device is in the idle state.
# After each reset, the state is changed to the next one as
# follow :
# - CRun
# - LP-Stop
# - Standby DDR self-refresh
# - Standby DDR off
# At fifth reset button press, the tests are finished and

FILE="/usr/workspace/power/smartfarming_power_testing_state.txt"
if [ ! -f "$FILE" ]; then
  echo "0" > $FILE
  chmod 666 $FILE
fi

# Read actual value from file
while read line; do
  value=$line
done < $FILE

# If there is a test left to do, increment and write back
# to prepare for next test. Otherwise delete file keeping 
# current state. 
if [ "$value" -lt "5" ]; then
  echo "$((value + 1))" > $FILE
else
  rm $FILE
  echo "SmartFarming power consumption tests finished."
fi

# The different power consumption tests accordingly to 
# point 3.4 Various low-power modes with OpenST Linux 
# in AN 5284 application note.
# https://www.st.com/resource/en/application_note/dm00595472-stm32mp1-series-system-power-consumption-stmicroelectronics.pdf
if [ "$value" = "1" ]; then   
  # CRun
  echo "CRun"
  while true; do echo "" > /dev/null; done
elif [ "$value" = "2" ]; then 
  # LP-Stop
  echo "LP-Stop"
  echo enabled > /sys/devices/platform/soc/40010000.serial/tty/ttySTM0/power/wakeup
  echo enabled > /sys/devices/platform/soc/40010000.serial/power/wakeup
  echo mem > /sys/power/state
elif [ "$value" = "3" ]; then 
  # Standby DDR self-refresh
  echo "Standby DDR self-refresh"
  echo mem > /sys/power/state
elif [ "$value" = "4" ]; then
  # Standby DDR off
  echo "Standby DDR off"
  shutdown -h 0
fi

exit 0