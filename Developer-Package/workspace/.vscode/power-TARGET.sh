#!/bin/bash
# It's easier to write a shell script than writing all in .vscode

##############################################################
### DEFAULT VALUES : can be changed with env variable

# Target address
default_target="root@192.168.4.2"
# Power dir path on host
default_power_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )/power"

##############################################################

# Path where the system dir is on the target
target_power_dir="/usr/workspace/power"
# Path where the services are placed on the target
target_services_dir="/etc/systemd/system/"

usage="Usage: $0 [-cdhl] \n
            -c\t\t        Clear (delete) all files from target\n
            -d\t\t        Deploy all files to target\n
            -h\t\t        Help information\n
            -l\t\t        Launch the services on target"
error_usage="Try '$0 -h' for more information."

# Analysis of options
while getopts cde:hl param
do
    has_option=true                         # 'true' if user used at least one option, '""' otherwise
    case $param in
        c)  clear_option=true                  
            ;;                              
        d)  deploy_option=true
            ;;
        h)  echo -e $usage
            exit 0
            ;;
        l)  launch_option=true
            ;;
        ?)  echo $error_usage
            exit 1
            ;;
    esac
done
shift "$(($OPTIND-1))"  # Removes the options from $OPTIND

if [ "$has_option" != true ]                # Verification if at least one option was used
then 
    echo "system-TARGET: at least one option must be used"
    echo $error_usage
    exit 2 
fi

if [ -z "${SMARTFARMING_TARGET}" ]          # The env variable doesn't exist
then
    target=$default_target
    echo "Setting target as (default) : $target"
else
    target=${SMARTFARMING_TARGET}
    echo "Setting target as (environment variable) : $target"
fi

if [ -z "${SMARTFARMING_POWER_DIR}" ]      # The env variable doesn't exist
then
    power_dir=$default_power_dir
    echo "Setting power directory path as (default) : $power_dir"
else
    power_dir=${SMARTFARMING_POWER_DIR}
    echo "Setting power directory path as (environment variable) : $power_dir"
fi

# Clear option : delete all file on the target
if [ "$clear_option" = true ]   
then
    echo "Disable services on target..."
    ssh $target 'systemctl disable smartfarming_power 2>/dev/null'
    
    echo "Deleting files on target..."
    ssh $target rm -f ${target_services_dir}/smartfarming_power.service
    ssh $target rm -rf ${target_power_dir}
fi

# Deploy option : deploy files on the target
if [ "$deploy_option" = true ]   
then
    echo "Copy files on target..."
    # Create necessary directories
    ssh $target mkdir -p ${target_power_dir}                            
    # Copy power directory
    scp -r $power_dir/* $target:${target_power_dir}                                   
    # Copy services
    ssh $target ln -s ${target_power_dir}/smartfarming_power.service ${target_services_dir}/smartfarming_power.service 

    echo "Enable services"
    ssh $target 'systemctl enable smartfarming_power' 
fi

# Launch option : launch services on the target
if [ "$launch_option" = true ]   
then
    echo "Launch services on the target..."
    ssh $target 'systemctl start smartfarming_power'
fi

echo "End"
exit 0

