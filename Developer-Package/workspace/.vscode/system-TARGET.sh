#!/bin/bash
# It's easier to write a shell script than writing all in .vscode

##############################################################
### DEFAULT VALUES : can be changed with env variable

# Target address
default_target="root@192.168.4.2"
# System dir path on host
default_system_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." &> /dev/null && pwd )/system"

##############################################################

# Path where the system dir is on the target
target_system_dir="/usr/workspace/system"
# Path where the services are placed on the target
target_services_dir="/etc/systemd/system/"

usage="Usage: $0 [-cdhl] [-e dir ] \n
            -c\t\t        Clear (delete) all files from target\n
            -d\t\t        Deploy all files to target\n
            -e\t\t        Export (copy) taken images on host (host directory must be given)\n
            -h\t\t        Help information\n
            -l\t\t        Launch the services on target"
error_usage="Try '$0 -h' for more information."

# Analysis of options
while getopts cde:hl param
do
    has_option=true                         # 'true' if user used at least one option, '""' otherwise
    case $param in
        c)  clear_option=true                  
            ;;                              
        d)  deploy_option=true
            ;;
        e)  export_option=true
            host_img_dir=$OPTARG            # Directory on host machine were pictures are copied
            ;;
        h)  echo -e $usage
            exit 0
            ;;
        l)  launch_option=true
            ;;
        ?)  echo $error_usage
            exit 1
            ;;
    esac
done
shift "$(($OPTIND-1))"  # Removes the options from $OPTIND

if [ "$has_option" != true ]                # Verification if at least one option was used
then 
    echo "system-TARGET: at least one option must be used"
    echo $error_usage
    exit 2 
fi

if [ -z "${SMARTFARMING_TARGET}" ]          # The env variable doesn't exist
then
    target=$default_target
    echo "Setting target as (default) : $target"
else
    target=${SMARTFARMING_TARGET}
    echo "Setting target as (environment variable) : $target"
fi

if [ -z "${SMARTFARMING_SYSTEM_DIR}" ]      # The env variable doesn't exist
then
    system_dir=$default_system_dir
    echo "Setting system directory path as (default) : $system_dir"
else
    system_dir=${SMARTFARMING_SYSTEM_DIR}
    echo "Setting system directory path as (environment variable) : $system_dir"
fi

# Clear option : delete all file on the target
if [ "$clear_option" = true ]   
then
    echo "Disable services on target..."
    ssh $target 'systemctl disable smartfarming_system_prelaunch 2>/dev/null'
    ssh $target 'systemctl disable smartfarming_system_wakeup 2>/dev/null'

    echo "Deleting files on target..."
    ssh $target rm -f ${target_services_dir}/smartfarming_system_*.service
    ssh $target rm -rf ${target_services_dir}/smartfarming_system_*.service.wants
    ssh $target rm -rf ${target_system_dir}
fi

# Deploy option : deploy files on the target
if [ "$deploy_option" = true ]   
then
    echo "Copy files on target..."
    # Create necessary directories
    ssh $target mkdir -p ${target_system_dir}                            
    ssh $target mkdir -p ${target_system_dir}/OpenAMP_wakeup_CM4/lib/firmware
    # Copy system directory
    scp -r $system_dir/* $target:${target_system_dir}                                   
    # Copy cortex-M app
    cortexm_app_file="$system_dir/../../STM32Cube_FW_MP1_V1.4.0/Projects/STM32MP157C-EV1/Applications/OpenAMP/OpenAMP_wakeup/STM32CubeIDE/CM4/Debug/OpenAMP_wakeup_CM4.elf"
    scp $cortexm_app_file $target:${target_system_dir}/OpenAMP_wakeup_CM4/lib/firmware
    # Copy services
    ssh $target ln -s ${target_system_dir}/scripts/smartfarming_system_prelaunch.service ${target_services_dir}/smartfarming_system_prelaunch.service 
    ssh $target ln -s ${target_system_dir}/scripts/smartfarming_system_wakeup.service ${target_services_dir}/smartfarming_system_wakeup.service 

    echo "Enable services"
    ssh $target 'systemctl enable smartfarming_system_prelaunch'
    ssh $target 'systemctl enable smartfarming_system_wakeup'  
fi

# Launch option : launch services on the target
if [ "$launch_option" = true ]   
then
    echo "Launch services on the target..."
    ssh $target 'systemctl start smartfarming_system_prelaunch'
    ssh $target 'systemctl start smartfarming_system_wakeup' 
fi

# Export option : copy images from target to host
if [ "$export_option" = true ]   
then
    echo "Copy images from target to host..."
    scp -r -p $target:${target_system_dir}/models/mobilenet/testdata/ $host_img_dir
fi

echo "End"
exit 0

