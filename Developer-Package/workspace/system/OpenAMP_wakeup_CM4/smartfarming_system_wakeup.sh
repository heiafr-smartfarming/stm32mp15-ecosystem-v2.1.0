#!/bin/sh

# Verify the status of the IPCC wakeup source
status=`cat /sys/devices/platform/soc/4c001000.mailbox/power/wakeup`
echo "Status of IPCC : ${status}"

# Verify the status of the application on the cortex-M
# Debug log : cat /sys/kernel/debug/remoteproc/remoteproc0/trace0
echo `cat /sys/kernel/debug/remoteproc/remoteproc0/trace0`

# Launch ML python script
echo "Launch ML python script"
source /usr/workspace/system/python/launch_python_label_tfl_mobilenet_testdata.sh

# Stop MCU
echo "Stop MCU"
echo "*stop" > /dev/ttyRPMSG0
echo `cat /sys/kernel/debug/remoteproc/remoteproc0/trace0`

# Stop MPU
# Debug log : journalctl -u systemd-suspend.service
echo "Stop MPU"
systemctl suspend