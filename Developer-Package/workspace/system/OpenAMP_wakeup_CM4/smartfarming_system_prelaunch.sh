#!/bin/sh

# Enable IPCC wakeup source
echo "Enable IPCC wakeup source" 
echo "enabled" > /sys/devices/platform/soc/4c001000.mailbox/power/wakeup
status=`cat /sys/devices/platform/soc/4c001000.mailbox/power/wakeup`
echo "Status of IPCC : ${status}"

# Launch application on the cortex-M
# Debug log : cat /sys/kernel/debug/remoteproc/remoteproc0/trace0
echo "Launch application on the cortex-M"
source /usr/workspace/system/OpenAMP_wakeup_CM4/fw_cortex_m4.sh start
echo `cat /sys/kernel/debug/remoteproc/remoteproc0/trace0`

# Stop MCU
# echo "Stop MCU"
# echo "*stop" > /dev/ttyRPMSG0
# echo `cat /sys/kernel/debug/remoteproc/remoteproc0/trace0`

# Stop MPU
# Debug log : journalctl -u systemd-suspend.service
# echo "Stop MPU"
# systemctl suspend