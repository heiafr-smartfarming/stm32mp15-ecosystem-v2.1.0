#!/usr/bin/python3

# Original program in : /usr/local/demo-ai/computer-vision/tflite-image-classification/python

import numpy as np
import argparse
import os
import cv2

from PIL import Image
import tflite_runtime.interpreter as tflr

from timeit import default_timer as timer

class VideoFrameCapture:
    """
    Class that handles video capture from device
    """
    def __init__(self, device=-1, width=320, height=240, fps=15):
        """
        :param device: device index or video filename (-1 for default device)
        :param width:  width of the requested frame
        :param height: heigh of the requested frame
        :param fps:    framerate of the camera
        """
        self._device = device
        self._width = width
        self._height = height
        self._fps = fps
        self._cap = cv2.VideoCapture(self._device)
        self._cap.set(cv2.CAP_PROP_FRAME_WIDTH, self._width)
        self._cap.set(cv2.CAP_PROP_FRAME_HEIGHT, self._height)
        self._cap.set(cv2.CAP_PROP_FPS, self._fps)
        assert self._cap.isOpened(), "SmartFarming : The camera could not be opened"
        assert self._cap.grab(), "SmartFarming : The child could not grab the video capture"

    def get_frame(self):
        """
        :return: frame
        """
        frame = None
        correct_frame = False
        fail_counter = -1
        while not correct_frame:
            # Capture the frame
            correct_frame, frame = self._cap.read()
            fail_counter += 1
            # Raise exception if there's no output from the device
            if fail_counter > 10:
                raise Exception("SmartFarming : Exceeded number of tries to capture "
                                "the frame.")
        return frame

    def get_frame_size(self):
        """
        :return: size of the captured image
        """
        return (int(self._height), int(self._width), 3)

    def release(self):
        self._cap.release()

class NeuralNetwork:
    """
    Class that handles Neural Network inference
    """
    def __init__(self, model_file, label_file, input_mean, input_std):
        """
        :param model_path:  .tflite model to be executedname of file containing labels")
        :param label_file:  name of file containing labels
        :param input_mean:  input_mean
        :param input_std:   input standard deviation
        """
        def load_labels(filename):
            my_labels = []
            input_file = open(filename, 'r')
            for l in input_file:
                my_labels.append(l.strip())
            return my_labels

        self._model_file = model_file
        self._label_file = label_file
        self._input_mean = input_mean
        self._input_std = input_std
        self._floating_model = False

        self._interpreter = tflr.Interpreter(self._model_file)
        self._interpreter.allocate_tensors()

        self._input_details = self._interpreter.get_input_details()
        self._output_details = self._interpreter.get_output_details()

        # Check the type of the input tensor
        if self._input_details[0]['dtype'] == np.float32:
            self._floating_model = True
            print("SmartFarming : Floating point Tensorflow Lite Model")

        self._labels = load_labels(self._label_file)

    def get_labels(self):
        return self._labels

    def get_floating_model(self):
        return self._floating_model

    def get_img_size(self):
        """
        :return: size of NN input image size
        """
        # NxHxWxC, H:1, W:2, C:3
        return (int(self._input_details[0]['shape'][1]),
                int(self._input_details[0]['shape'][2]),
                int(self._input_details[0]['shape'][3]))

    def launch_inference(self, img):
        # Add N dim
        input_data = np.expand_dims(img, axis=0)

        if self._floating_model:
            input_data = (np.float32(input_data) - self._input_mean) / self._input_std

        self._interpreter.set_tensor(self._input_details[0]['index'], input_data)

        self._interpreter.invoke()

    def display_results(self):
        # Display output results
        output_data = self._interpreter.get_tensor(self._output_details[0]['index'])
        results = np.squeeze(output_data)

        top_k = results.argsort()[-5:][::-1]
        for i in top_k:
            if self._floating_model:
                print('{0:08.6f}'.format(float(results[i]))+":", self._labels[i])
            else:
                print('{0:08.6f}'.format(float(results[i]/255.0))+":", self._labels[i])
        print("\n")

        if self._floating_model:
            return (results[top_k[0]], top_k[0])
        else:
            return (results[top_k[0]]/255.0, top_k[0])

def camera_capture(camera, args):
    """
    Function captures a frame and write it on SD card
    :param camera: capture device
    :param args: application arguments
    :return: frame, or None if capture failed
    """
 
    frame = None
    frame = camera.get_frame()
    if frame is not None :
        path_output_dir = args.image
        value = len([names for names in os.listdir(os.path.join(path_output_dir)) 
                if os.path.isfile(os.path.join(path_output_dir, names))])
        print("SmartFarming value : ", value)
        res = cv2.imwrite(os.path.join(path_output_dir, 'image%d.jpg' % (value)), frame)
        if res:
            print("SmartFarming : Image no %d successfully written!" % (value))
    
    return frame

def nn_processing(nn, camera_frame):
    """
    Function processes received frame
    :param nn: neural network
    :param camera_frame: frame received from camera
    :return: nothing
    """

    shape = nn.get_img_size()
    nn_frame = cv2.resize(camera_frame, (shape[1], shape[0]))

    # Transform the nn_img array into image for NN
    img = Image.fromarray(nn_frame)

    start_time = timer()

    # Execute NN inference
    nn.launch_inference(img)

    # Compute inference time
    stop_time = timer()
    inference_time = stop_time - start_time

    # Get NN inference results
    accuracy, label = nn.display_results()

    return (inference_time, accuracy, label)


if __name__ == '__main__':

    print("SmartFarming : Start")

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--image", default="", help="image directory with image to be classified")
    parser.add_argument("-v", "--video_device", default=-1, help="video device (default /dev/video0)")
    parser.add_argument("--frame_width", default=320, help="width of the camera frame (default is 320)")
    parser.add_argument("--frame_height", default=240, help="height of the camera frame (default is 240)")
    parser.add_argument("--framerate", default=15, help="framerate of the camera (default is 15fps)")
    parser.add_argument("-m", "--model_file", default="", help=".tflite model to be executed")
    parser.add_argument("-l", "--label_file", default="", help="name of file containing labels")
    parser.add_argument("--input_mean", default=127.5, help="input mean")
    parser.add_argument("--input_std", default=127.5, help="input standard deviation")
    args = parser.parse_args()

    # Capture an image
    camera = VideoFrameCapture(args.video_device, args.frame_width, args.frame_height, args.framerate)
    frame = camera_capture(camera, args)
    camera.release()

    # Classification of the image
    nn = NeuralNetwork(args.model_file, args.label_file, float(args.input_mean), float(args.input_std))
    ### The four following lines permit to test with static image.
    ### It should give same result as with the original version.
    # shape = nn.get_img_size()
    # frame = Image.open(args.image + "/" + "bananen.jpg")
    # frame = cv2.resize(np.array(frame), (320, 240))
    # frame = cv2.resize(np.array(frame), (shape[1], shape[0]))
    (inference_time, accuracy, label) = nn_processing(nn, frame)

    print("SmartFarming : Processing time = %.3f s" % inference_time)
    if nn.get_floating_model:
        print('SmartFarming : {0:08.6f}'.format(float(accuracy))+":", nn.get_labels()[label])
    else:
        print('SmartFarming : {0:08.6f}'.format(float(accuracy/255.0))+":", nn.get_labels()[label])

    print("SmartFarming : Exit")
    